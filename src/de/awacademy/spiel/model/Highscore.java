package de.awacademy.spiel.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

public class Highscore {


    BufferedReader in = null;
    boolean allowHighscoreInput = true;
    String[] highscoreArray = new String[5];


    public void setAllowHighscoreInput(boolean allowHighscoreInput) {
        this.allowHighscoreInput = allowHighscoreInput;
    }

    public Highscore() {

        this.readHighscore("highscore.txt");
    }

    public String[] getHighscoreArray() {
        return highscoreArray;
    }

    public void show() {

        //   System.out.println(in.lines().toString());

    }


    public void readHighscore(String datName) {

        File file = new File(datName);

//        if (!file.canRead() || !file.isFile())
//            System.exit(0);


        try {
            in = new BufferedReader(new FileReader(datName));

            String input = in.readLine().replace("[","").replace("]","");

            String[] stringArray = new String[5];

            stringArray = input.split(",");

//            for(String item : highscoreArray){
//             //   item.replace(",","");
//                item=item.trim();
//            }
            for(int i=0; i<5;i++){

                this.highscoreArray[i] = stringArray[i].trim();
            }




//            String zeile = null;
//            while ((zeile = in.readLine()) != null) {
                //  System.out.println("Gelesene Zeile: " + zeile);
//            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                }
        }
    }


    public void updateHighscore(int newhighscore) {



        if (newhighscore > 0) {//Integer.parseInt(bestscore)){
           // newhighscore = 66;
            for (int i = 0; i < 5; i++) {

                if (allowHighscoreInput) {
                    if (highscoreArray[i].matches("[0-9]+")) {
                        if (newhighscore > Integer.parseInt(highscoreArray[i])) {

                            for (int j = 4; j > i; j--) {

                                highscoreArray[j] = highscoreArray[j - 1];
                            }

                            highscoreArray[i] = Integer.toString(newhighscore);
                            allowHighscoreInput = false;
                        }
                    } else {
                        System.err.println("nicht erlaubt");
                    }
                }
            }
        }


        try {
            writeHighscore();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public void writeHighscore() throws IOException {


        String highscore = Arrays.toString(highscoreArray);


        Files.write(Paths.get("./highscore.txt"), highscore.getBytes(),
                StandardOpenOption.CREATE);

    }
}
