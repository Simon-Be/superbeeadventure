package de.awacademy.spiel.model;

import de.awacademy.spiel.InputHandler;
import de.awacademy.spiel.Timer;
import javafx.scene.image.Image;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static de.awacademy.spiel.Main.HEIGHT;
import static de.awacademy.spiel.Main.WIDTH;


public class Model {

    private int counter = 0;
    private double moveHero = 5;
    private double moveEnemy = 5;
    private int startEnemies = 40;
    private int points = 10;

    private int status = 1;
    private int darknessSetter = 1;


    private Hero hero;
    private Treasure treasure;
    private Timer timer;
    private Darkness darkness;


    private int enemyCounter;

    private Highscore highscore;
    private int newHighscore = 0;


    double startheroX = 10;
    double startheroY = 10;
    double darknessX = startheroX - 890;
    double darknessY = startheroY - 890;
    double darknessRad = 1400;


    double enemyposX;
    double enemyposY;

    Random random = new Random();

    private boolean upPressed;
    private boolean downPressed;
    private boolean rightPressed;
    private boolean leftPressed;

    // Background

    public Image getBackImage() {
        try {
            // Image image = new Image(getClass().getResourceAsStream("Biene.jpg"));
            Image image = new Image(getClass().getResourceAsStream("back.jpg"));

            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    // Highscore

    public void newHighscore() {

        Highscore highscore = new Highscore();
        this.highscore = highscore;
        newHighscore = 0;
    }

    public String getGameHighscore(int i) {

        String[] highScoreArray = highscore.getHighscoreArray();


        return highScoreArray[i];
    }

    public int getNewHighscore() {
        return newHighscore;
    }

    // Darkness:


    public void newShadow() {

        Darkness darkness = new Darkness(darknessX, darknessY);
        this.darkness = darkness;
    }

    public double getShadowX() {
        return darkness.getPosX();
    }

    public double getShadowY() {
        return darkness.getPosY();
    }

    public int getDarknessSetter() {
        return darknessSetter;
    }

    public double getDarknessRad() {
        return darknessRad;
    }


    public void changeDarkness() {

        if (darknessSetter == 0) {
            this.darknessSetter = 1;
        } else {
            this.darknessSetter = 0;
        }


    }

    // Enemy:


    public int getenemyCounter() {
        return enemyCounter;
    }

    private List<Enemy> enemyList = new LinkedList<>();

    public void newEnemy() {

        double posX = random.nextDouble() * WIDTH;
        double posY = random.nextDouble() * HEIGHT;

        if (posX < 40) {
            posX += 40;
        }
        if (posY < 40) {
            posY += 40;
        }

        enemyList.add(new Enemy(posX, posY));
        enemyCounter++;
    }

    private void removeEnemies() {
        if (enemyCounter > startEnemies) {
            for (int i = 0; i < enemyCounter - startEnemies; i++) {

                enemyList.remove(0);


            }

        }
    }

    public void reduceEnemy(){
        enemyList.remove(0);
        enemyCounter --;
    }

    public List<Enemy> getEnemy() {

        return enemyList;
    }

    public int getStartEnemies() {
        return startEnemies;
    }

    public Image getenemyImage() {
        try {
            // Image image = new Image(getClass().getResourceAsStream("Biene.jpg"));
            Image image = new Image(getClass().getResourceAsStream("newBee2.png"));

            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // Hero:

    public void newHero() {

        Hero hero = new Hero(startheroX, startheroY);
        this.hero = hero;
    }

    public double getHeroX() {
        return hero.getPosX();
    }

    public double getHeroY() {
        return hero.getPosY();
    }

    public void setDownPressed(boolean downPressed) {
        this.downPressed = downPressed;
    }

    public void setUpPressed(boolean upPressed) {
        this.upPressed = upPressed;
    }

    public void setRightPressed(boolean rightPressed) {
        this.rightPressed = rightPressed;
    }

    public void setLeftPressed(boolean leftPressed) {
        this.leftPressed = leftPressed;
    }


    public void moveHeroplusX() {
        if (hero.getPosX() < 1170) {
            this.hero.setPosX(hero.getPosX() + moveHero);
            this.darkness.setPosX(darkness.getPosX() + moveHero);
        }
    }

    public void moveHerominusX() {
        if (hero.getPosX() > 10) {
            hero.setPosX(hero.getPosX() - moveHero);
            this.darkness.setPosX(darkness.getPosX() - moveHero);
        }
    }

    public void moveHeroplusY() {
        if (hero.getPosY() > 10) {
            this.hero.setPosY(hero.getPosY() - moveHero);
            this.darkness.setPosY(darkness.getPosY() - moveHero);
        }
    }

    public void moveHeroMinusY() {
        if (hero.getPosY() < 770) {
            hero.setPosY(hero.getPosY() + moveHero);
            darkness.setPosY(darkness.getPosY() + moveHero);
        }
    }

    public Image getheroImage() {
        try {
            Image image = new Image(getClass().getResourceAsStream("hummingBird5.png"));
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    // Treasure:

    public void newTreasure() {
        do {
            enemyposX = random.nextDouble() * WIDTH - 40;
            enemyposY = random.nextDouble() * HEIGHT - 40;
        }
        while (enemyposX < 600 || enemyposY < 400);

        Treasure treasure = new Treasure(enemyposX, enemyposY);
        this.treasure = treasure;
    }

    public double getTreasureX() {
        return treasure.getPosX();
    }

    public double getTreasureY() {
        return treasure.getPosY();
    }

    public void changeTreasure() {
        do {
            enemyposX = random.nextDouble() * WIDTH - 40;
            enemyposY = random.nextDouble() * HEIGHT - 40;
        }
        while (enemyposX < 50 || enemyposY < 50);

        treasure.setPosX(enemyposX);
        treasure.setPosY(enemyposY);
    }

    public void resetTreasure() {
        do {
            enemyposX = random.nextDouble() * WIDTH - 40;
            enemyposY = random.nextDouble() * HEIGHT - 40;
        }
        while (enemyposX < 600 || enemyposY < 400);

        Treasure treasure = new Treasure(enemyposX, enemyposY);
        this.treasure = treasure;
    }

    public Image gettreasureImage() {
        try {
            Image image = new Image(getClass().getResourceAsStream("honey5.png"));
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // Timer:
    public int getCounter() {
        return counter;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void update(long deltaMillis) {
        counter += deltaMillis;


        if (downPressed == true) {
            this.moveHeroMinusY();
        }

        if (upPressed == true) {
            this.moveHeroplusY();
        }

        if (rightPressed == true) {
            this.moveHeroplusX();
        }

        if (leftPressed == true) {
            this.moveHerominusX();

        }
        for (Enemy item : enemyList) {

            if ((hero.getPosX() >= item.getPosX()
                    && hero.getPosX() <= item.getPosX() + 30
                    && hero.getPosY() <= item.getPosY() + 30
                    && hero.getPosY() >= item.getPosY())

                    || (hero.getPosX() + 20 >= item.getPosX()
                    && hero.getPosX() + 20 <= item.getPosX() + 30
                    && hero.getPosY() <= item.getPosY() + 30
                    && hero.getPosY() >= item.getPosY())

                    || (hero.getPosX() + 20 >= item.getPosX()
                    && hero.getPosX() + 20 <= item.getPosX() + 30
                    && hero.getPosY() + 20 <= item.getPosY() + 30
                    && hero.getPosY() + 20 >= item.getPosY())

                    || (hero.getPosX() >= item.getPosX()
                    && hero.getPosX() <= item.getPosX() + 30
                    && hero.getPosY() + 20 <= item.getPosY() + 30
                    && hero.getPosY() + 20 >= item.getPosY())) {

                //System.out.println("Berührt");


                status = 2;
                highscore.show();
                highscore.updateHighscore(newHighscore);

            }
        }
        if ((hero.getPosX() >= treasure.getPosX()
                && hero.getPosX() <= treasure.getPosX() + 30
                && hero.getPosY() <= treasure.getPosY() + 30
                && hero.getPosY() >= treasure.getPosY())

                || (hero.getPosX() + 20 >= treasure.getPosX()
                && hero.getPosX() + 20 <= treasure.getPosX() + 30
                && hero.getPosY() <= treasure.getPosY() + 30
                && hero.getPosY() >= treasure.getPosY())

                || (hero.getPosX() + 20 >= treasure.getPosX()
                && hero.getPosX() + 20 <= treasure.getPosX() + 30
                && hero.getPosY() + 20 <= treasure.getPosY() + 30
                && hero.getPosY() + 20 >= treasure.getPosY())

                || (hero.getPosX() >= treasure.getPosX()
                && hero.getPosX() <= treasure.getPosX() + 30
                && hero.getPosY() + 20 <= treasure.getPosY() + 30
                && hero.getPosY() + 20 >= treasure.getPosY())) {

            //System.out.println("gewonnen");
            this.newEnemy();
            this.changeTreasure();

            newHighscore += points;

        }


        for (Enemy item : enemyList) {

            switch (random.nextInt(4)) {

                case 1:
                    if (item.getPosX() < 1155) {
                        item.setPosX(item.getPosX() + moveEnemy);
                    }
                    break;
                case 2:
                    if (item.getPosX() > 45) {
                        item.setPosX(item.getPosX() - moveEnemy);
                    }
                    break;
                case 3:
                    if (item.getPosY() > 45) {
                        item.setPosY(item.getPosY() - moveEnemy);
                    }
                    break;
                case 0:
                    if (item.getPosY() < 755) {
                        item.setPosY(item.getPosY() + moveEnemy);
                    }
                    break;
            }
        }
    }


    public void resetCounter() {
        counter = 0;
    }

    public void resetGame() {
        resetCounter();
        hero.setPosX(startheroX);
        hero.setPosY(startheroY);
        resetTreasure();
        darkness.setPosX(darknessX);
        darkness.setPosY(darknessY);
        removeEnemies();
        enemyCounter = startEnemies;
        newHighscore = 0;
        highscore.setAllowHighscoreInput(true);
        setStatus(1);
    }

    public void raiseDifficulty() {

        moveEnemy -= 1;
        darknessRad -= 100;

        if (points > 0) {
            points -= 5;
        }
    }

    public void lowerDifficulty() {

        moveEnemy += 1;
        darknessRad += 100;
        points += 5;

    }


}
