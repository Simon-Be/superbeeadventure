package de.awacademy.spiel.model;

import javafx.scene.image.Image;

import java.util.Random;

public class Enemy {
    private double posX;
    private double posY;
    Random random = new Random();


    public Enemy(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public double getPosX() {
        return posX;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }


}
