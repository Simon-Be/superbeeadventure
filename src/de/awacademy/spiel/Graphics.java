package de.awacademy.spiel;

import de.awacademy.spiel.model.Enemy;
import de.awacademy.spiel.model.Highscore;
import de.awacademy.spiel.model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BoxBlur;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import static de.awacademy.spiel.Main.*;


public class Graphics {

    private Model model;

    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;

    }



    public void draw(int status) {

        switch (status) {


            case 0:





                break;

        case 1:
            //Scene
            gc.setFill(Color.SANDYBROWN);
            gc.fillRect(0, 0, 1200, 800);

//            gc.setFill(Color.WHITE);
//            gc.fillRect(10, 10, 1180, 780);

            gc.drawImage(model.getBackImage(),10, 10, 1180, 780);

            gc.setFill(Color.BROWN);
            gc.fillRoundRect(0, 0, 50, 50,10,10);

            // hier mehr Mauern

            gc.setFill(Color.LIGHTGREY);
            gc.fillRect(0, 0, 40, 40);

            //Hero

            gc.drawImage(model.getheroImage(), model.getHeroX(), model.getHeroY(), 20, 20);

            // Treasure

            gc.drawImage(model.gettreasureImage(),model.getTreasureX(), model.getTreasureY(), 30, 30);

            // Enemy

            for (Enemy item : model.getEnemy()) {
                //Image image = model.getImage();
                gc.drawImage(model.getenemyImage(), item.getPosX(), item.getPosY(), 30, 30);
            }

            // Darkness

            if ( model.getDarknessSetter() == 1) {
                //gc.setEffect(new BoxBlur( 2,  2, 3));
                gc.setStroke(Color.BLACK);
             //  gc.setStroke(Color.BURLYWOOD);
             //   gc.setStroke(new Color(0.5,0.5,1,1));
              //  gc.setStroke(gc.drawImage();
                gc.setLineWidth(model.getDarknessRad());
                gc.strokeOval(model.getShadowX(), model.getShadowY(), 1800, 1800);
            }

            gc.setFill(Color.RED);
            gc.setFont(new Font("Arial", 20));
            gc.fillText("Score:  " + model.getNewHighscore(), 60, 30);
            gc.fillText("Bees:  " + model.getenemyCounter(), 360, 30);

            break;

            case 2:

                gc.setFill(Color.BLACK);
                gc.fillRect(0, 0, 1200, 800);

                gc.setFont(new Font("Serif", 80));
                gc.setFill(Color.BLUEVIOLET);
                gc.fillText("Du wurdest gestochen",200,100);

                gc.setFill(Color.DARKGRAY);
                // Try again Button
                gc.fillRect(buttonposX,buttonposY,buttonwide,buttonheight);
                // Exit Button
                gc.fillRect(buttonposX+400,buttonposY,buttonwide,buttonheight);

                gc.setFont(new Font("Serif", 30));
                gc.setFill(Color.BLUEVIOLET);
                gc.fillText("Try Again",buttonposX+35,buttonposY+60);
                gc.setFill(Color.BLUEVIOLET);
                gc.fillText("Exit",buttonposX+475,buttonposY+60);

                gc.setFont(new Font("Serif", 40));
                gc.fillText("Dein Score:  "+ model.getNewHighscore(),buttonposX+35,buttonposY+180);
                gc.setFont(new Font("Serif", 50));
                gc.fillText("Highscores",buttonposX+175,buttonposY+280);

                gc.setFont(new Font("Serif", 40));
                gc.fillText("1.  :  "+ model.getGameHighscore(0),buttonposX+35,buttonposY+340);

                gc.fillText("2.  :  "+ model.getGameHighscore(1),buttonposX+35,buttonposY+380);

                gc.fillText("3.  :  "+ model.getGameHighscore(2),buttonposX+35,buttonposY+420);

                gc.fillText("4.  :  "+ model.getGameHighscore(3),buttonposX+35,buttonposY+460);

                gc.fillText("5.  :  "+ model.getGameHighscore(4),buttonposX+35,buttonposY+500);


                break;
        }
    }
}
