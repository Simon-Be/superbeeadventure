package de.awacademy.spiel;

import de.awacademy.spiel.model.Model;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;


import static de.awacademy.spiel.Main.*;

public class InputHandler {
    private Model model;


    int status;

    public InputHandler(Model model) {
        this.model = model;
    }


    public void onkeyReleased(KeyEvent event) {

        switch (event.getCode()) {
            case NUMPAD8:    // UP
                model.setUpPressed(false);
                break;
            case NUMPAD5:    // DOWN
                model.setDownPressed(false);
                break;
            case NUMPAD6:         // RIGHT
                model.setRightPressed(false);
                break;
            case NUMPAD4:          // LEFT
                model.setLeftPressed(false);
                break;


        }
    }

    public void onkeyPressed(KeyEvent event) {

        status = model.getStatus();

        switch (status) {

            case 1:

                switch (event.getCode()) {
                    case NUMPAD8:    // UP
                        model.setUpPressed(true);
                        break;
                    case NUMPAD5:    // DOWN
                        model.setDownPressed(true);
                        break;
                    case NUMPAD6:         // RIGHT
                        model.setRightPressed(true);
                        break;
                    case NUMPAD4:          // LEFT
                        model.setLeftPressed(true);
                        break;
                    case SPACE:
                        //model.setSpacePressed(true);
                        model.changeDarkness();
                        break;
                    case V:          // LEFT
                        model.raiseDifficulty();
                        break;
                    case C:          // LEFT
                        model.lowerDifficulty();
                        break;
                    case D:          // moreBees
                        model.newEnemy();
                        break;
                    case F:          // reduceBees
                        model.reduceEnemy();
                        break;
                }
                break;

            case 2:
                break;
        }

    }

    public void onClick(MouseEvent event) {

        status = model.getStatus();

        switch (status) {


            case 2:

                if (event.getX() >= buttonposX && event.getX() <= buttonposX + buttonwide && event.getY() >= buttonposY && event.getY() <= buttonposY + buttonheight) {
                    System.out.println("reset");
                    model.resetGame();


                } else if (event.getX() >= buttonposX + 400 && event.getX() <= buttonposX + 400 + buttonwide && event.getY() >= buttonposY && event.getY() <= buttonposY + buttonheight) {
                    System.out.println("exit");
                    System.exit(0);
                }
                break;
        }
    }


}




/*
    public void onClick(MouseEvent event) {
        model.newHero(event.getX(), event.getY());
    }
}
*/