package de.awacademy.spiel;

import de.awacademy.spiel.model.Hero;
import de.awacademy.spiel.model.Model;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {



    public static final double WIDTH = 1200;
    public static final double HEIGHT = 800;

    public static final double buttonposX = 300;
    public static final double buttonposY = 200;
    public static final double buttonwide = 200;
    public static final double buttonheight = 100;

    private Timer timer;





    @Override
    public void stop() throws Exception {
        super.stop();
        timer.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Canvas canvas = new Canvas(WIDTH, HEIGHT);

        Group group = new Group();
        group.getChildren().addAll(canvas);

        Scene scene = new Scene(group);
        primaryStage.setScene(scene);
        primaryStage.show();

        Model model = new Model();
        Graphics graphics = new Graphics(model, canvas.getGraphicsContext2D());
        timer = new Timer(model, graphics);

        InputHandler inputHandler = new InputHandler(model);

        model.newHero();
        model.newTreasure();

        model.newShadow();
        model.newHighscore();


        for (int x=0;x<model.getStartEnemies();x++){
            model.newEnemy();
        }
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                inputHandler.onkeyPressed(event);
            }
        });

        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                inputHandler.onkeyReleased(event);
            }
            });
        canvas.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                inputHandler.onClick(event);

            }
        });
        timer.start();
        /*
        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                inputHandler.onkeyReleased(event);
            }
        });
        */


    }
}
